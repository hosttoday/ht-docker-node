FROM ubuntu:20.04
LABEL author="Task Venture Capital GmbH <hello@task.vc>"

WORKDIR /workspace
# important environment variables 
ENV NODE_VERSION_LTS="20.12.2" NODE_VERSION_STABLE="20.12.2" NVM_DIR="/usr/local/nvm"

# Set debconf to run non-interactively and install packages
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
    && apt-get update \
    && apt-get upgrade --no-install-recommends -y \
    && apt-get install -y -q --no-install-recommends \
        # base libs
        software-properties-common \
        apt-transport-https \
        build-essential \
        ca-certificates \
        gpg-agent \
        curl \
        g++ \
        gcc \
        git \
        make \
        openssl \
        python \
        python3 \
        rsync \
        ssh \
        wget \
        # puppeteer
        gconf-service \
        libasound2 \
        libatk1.0-0 \
        libc6 \
        libcairo2 \
        libcups2 \
        libdbus-1-3 \
        libexpat1 \
        libfontconfig1 \
        libgcc1 \
        libgconf-2-4 \
        libgdk-pixbuf2.0-0 \
        libglib2.0-0 \
        libgtk-3-0 \
        libnspr4 \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        libstdc++6 \
        libx11-6 \
        libx11-xcb1 \
        libxcb1 \
        libxcomposite1 \
        libxcursor1 \
        libxdamage1 \
        libxext6 \
        libxfixes3 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxss1 \
        libxtst6 \
        ca-certificates \
        fonts-liberation \
        libappindicator1 \
        libnss3 \
        lsb-release \
        xdg-utils \
        # network
        iputils-ping \
        dnsutils \
    
    # chrome
    && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && apt install -y -q --no-install-recommends ./google-chrome-stable_current_amd64.deb \
    
    # mongodb
    && curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add - \
    && echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.4.list \
    && apt-get update \
    && apt-get install -y -q --no-install-recommends \
        mongodb-org \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

# Install nvm with node and npm
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash

# prepare pnpm
ENV PNPM_HOME="/root/.local/share/pnpm/pnpm"
RUN mkdir -p ${PNPM_HOME}
ENV PATH="$PNPM_HOME:$PATH"

RUN bash -c "source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION_STABLE \
    && nvm alias default $NODE_VERSION_STABLE \
    && nvm use default \
    && curl -fsSL https://get.pnpm.io/install.sh | bash - \
    && pnpm -v \
    && pnpm config set unsafe-perm true"

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION_STABLE/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION_STABLE/bin:$PATH
